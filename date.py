"""import requests  
from bs4 import BeautifulSoup 
 
# Making get request and storing the response in a variable 
webpage = requests.get('https://skill-lync.com/') 
 
#Parsing the content of web page by html parser 
soup = BeautifulSoup(webpage.content, 'html.parser') 
 
#Finding all meta tags present, stored in a list format 
meta_tag = soup.findAll('meta') 
 
# Looping the meta tag list 
for x in meta_tag: 
    print(meta_tag)
    print('-----------------') 
    print(x.attrs["content"])"""
"""from bs4 import BeautifulSoup
import requests


def main():
    #r = requests.get('http://www.aurionpro.com/')
    r = requests.get('http://www.google.com/')
    soup = BeautifulSoup(r.content, 'html.parser')
    

    title = soup.title.string
    print('TITLE IS :', title)

    meta = soup.findAll('meta') 
    print("", meta)

    for tag in meta:
        #if 'name' in tag.attrs.keys() and tag.attrs['name'].strip().lower() in ['description', 'keywords']:
            #print('NAME    :',tag.attrs['name'].lower())
            print('CONTENT :',tag.attrs['content'])

if __name__ == '__main__':
    main()"""
    
"""import requests
from bs4 import BeautifulSoup

url = "https://skill-lync.com"
req = requests.get(url)
soup = BeautifulSoup(req.text, "html.parser", from_encoding=url.info().get_param("charset"))
print(soup.title)
for link in soup.find_all("meta"):
    #print(link.attrs['content'])
    print(link)"""

import urllib.request
from urllib.parse import urlparse
from bs4 import BeautifulSoup

response = urllib.request.urlopen("https://skill-lync.com/")
soup = BeautifulSoup(response,'html.parser', from_encoding=response.info().get_param('charset'))
for link in soup.find_all("meta"):
    #print(link.attrs['content'])
    print(link)
if soup.findAll("meta", attrs={"name": "description"}):
    print("Meta Data is :")
    print(soup.find("meta", attrs={"name": "description"}).get("content"))
else:
    print("error")

if soup.findAll("title"):
    print("Title is: ")
    print(soup.find("title").string)
else:
    print("error")